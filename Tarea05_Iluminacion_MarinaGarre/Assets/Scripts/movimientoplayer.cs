using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimientoplayer : MonoBehaviour
{
    public Rigidbody2D riggidbody2d;
    public float vel = 0.01f;
    public float vel_salto = 0.01f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            transform.position += Vector3.left * vel;
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.position += Vector3.right * vel;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
           riggidbody2d.velocity += Vector2.up * vel_salto;
        }
    }
}
